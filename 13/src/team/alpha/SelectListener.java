package team.alpha;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import static team.alpha.PLAYS.*;

public class SelectListener implements OnClickListener {
	GameActivity gAct;
	// the index of the button this card is on
	public int bIndex;
	
	public SelectListener(int i, GameActivity gameActivity) {		
		this.bIndex = i;
		this.gAct = gameActivity;    
	}
	
	// precondition: called by Android OS upon user clicking card button
	// postcondition: sets clicked[] to reflect this button's state
	//		and calls GameInterface.playerSelect() or playerUnselect()
	public void onClick(View v) {
		// get the card value
		int cardVal = gAct.visibleCards[bIndex];
		
		// not clicked so choose
		if(gAct.clicked[bIndex] == false) {
			gAct.buttons[bIndex].startAnimation(gAct.up[bIndex]);
			gAct.clicked[bIndex] = true;		
			
			// run selection code
			gAct.gInterf.playerSelect(cardVal, bIndex);
		} else {	// already clicked, so unchoose
			gAct.buttons[bIndex].startAnimation(gAct.down[bIndex]);
			gAct.clicked[bIndex] = false;
			
			// unchoose this card
			gAct.gInterf.playerUnselect(cardVal, bIndex);
		}
	}
}