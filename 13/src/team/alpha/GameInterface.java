package team.alpha;

import static team.alpha.PLAYS.*;

public class GameInterface {
	// reference to the android GameActivity
	transient GameActivity gAct;
	// reference to the Game object
	public Game game;
	
	// by default human is player #0
	public final int HUMAN = 0;
	
	public int roundState;
	// ROUND STATE CONSTANTS
	public final int ROUNDWON = -1;
	public final int INITGAME = -2;
	public final int SKIPPED = -3;
	public final int CONTINUEROUND = -4;
	public final int GAMEOVER = -5;
	
	final public int[] cardsChosen = new int[13];
	static final int[] temp = new int[13];
	public int currentPlay = -1;					// holds play type of the cards that are chosen, can display this in GUI
	public int currInd = 0; 
	public int[] indexChosen = new int[13];
	
	public GameInterface(GameActivity ga) {
		this.gAct = ga;
		
		for (int i=0; i<13; i++) {
			cardsChosen[i] = -1;
			temp[i] = -1;
			indexChosen[i] = -1;
		}
		currInd = 0;
	}

	/////////////////////////////////////////////////////////////////
	// GAMPLAY METHODS
	/////////////////////////////////////////////////////////////////
	
	// precondition: none
	// postcondition: initializes game object, start the game loop
	public void startGame() {
		// create Game object and deal cards
		game = new Game(this);
		game.dealFairly();
		game.init();
		
		// initialize humans cards on GUI
		gAct.initHumansHand( game.hands[HUMAN] );
		
		// determine if human initiates game
		if (game.turn != HUMAN) {
			// disable buttons so human doesn't crash program
			gAct.disablePlayerButtons();
			// do the AI turns, animate, etc..
			game.runAIRounds();
			setupPlayerTurn();
		} else {
			// set the variables so player can initiate game
			roundState = INITGAME;
			setButtons();
		}
	}
	
	// precondition: none
	// postcondition: reads game object, sets roundState
	public void determineState() {
		if (!game.gameWon) {
			int numSkipped = 0;
			boolean roundWon = false;
			// calculate how many players skipped
			for (int j=0; j<4; j++) if (game.skipTurn[j]) numSkipped++;
			// if 3 AI's skipped, then the round was won
			if (numSkipped == 3) roundWon = true;
			// if player won that round, they set the new round type
			if (roundWon) {
				// reset skipTurn[]
				for (int j=0; j<4; j++) game.skipTurn[j] = false;
				roundState = ROUNDWON;
				gAct.clearSkipButtons();
				gAct.clearCurrentPlayed();
			} else {
				// continue round
				roundState = CONTINUEROUND;
			}
		} else {
			roundState = GAMEOVER;
		}
	}
	
	// precondition: round state determined
	// postcondition: sets up the buttons according to state
	public void setButtons() {
		switch (roundState) {
		// human won the round
		case ROUNDWON:
			// can't skip turn if initiating round
			gAct.passButtonOFF();
			gAct.enablePlayerButtons();
			
			// clear all the skip buttons
			gAct.clearSkipButtons();
			
			break;
		case CONTINUEROUND:
			// make player buttons clickable again
			gAct.enablePlayerButtons();
			
			// enable pass button
			gAct.passButtonON();
		break;
		case INITGAME:
			gAct.enablePlayerButtons();
			break;
		case GAMEOVER:
			gAct.disablePlayerButtons();
			gAct.playButtonOFF();
			gAct.passButtonOFF();
			break;
		}
	}
	
	// precondition: none
	// postcondition: sets up the interface for user's turn
	public void setupPlayerTurn() {
		determineState();
		setButtons();
		
		// unchoose all cards
		for (int i=0; i<13; i++) {
			cardsChosen[i] = -1;
			indexChosen[i] = -1;
		}
		currInd = 0;
	}
	
	// precondition: cardsChosen[], indexChosen[] sifted & sorted
	// postcondition: depending on state of round, checks if the cards
	//		match the play needed, and enables/disables play button accordingly
	public void checkChosenUpdatePlayButton() {
		boolean playStronger = false;
		currentPlay = NOPLAY;
		switch (roundState) {
		// initiating round, can make any play
		case ROUNDWON:
			if ((currentPlay = Game.isPlay(cardsChosen)) != NOPLAY)
				gAct.playButtonON();
			else
				gAct.playButtonOFF();
			break;
		// play must match previous player's
		case CONTINUEROUND:
			if (Game.isCorrectPlay(cardsChosen, game.roundType)) {
				playStronger = cardsChosen[0] < game.current[0];
				if (playStronger) {
					gAct.playButtonON();
					currentPlay = game.roundType;
				}
			}
			else
				gAct.playButtonOFF();
			break;
		// play must include the lowest card #51
		case INITGAME:
			if (Game.isInitialPlay(cardsChosen)) {
				gAct.playButtonON();
				currentPlay = Game.isPlay(cardsChosen);
			}
			else
				gAct.playButtonOFF();
			break;
		}
	}
		
	/////////////////////////////////////////////////////////////////
	// GUI CALLBACK METHODS (LISTENER METHODS)
	/////////////////////////////////////////////////////////////////
	
	// precondition: called from skip button
	// postcondition: updates GUI , Game object for skipping turn
	public void playerSkipTurn() {
		// set skip button
		gAct.skipTurn(HUMAN);
		
		game.skipTurn[HUMAN] = true;
		
		// increment turn
		game.turn = (game.turn + 1) % 4;
		
		// run AI turns
    	game.runAIRounds();
    	
    	// prepare GUI for next human turn
    	setupPlayerTurn();
	}
	
	// precondition: called from selecting a card
	// postcondition: updates interface with selection
	public void playerSelect(int val, int bIndex) {
		// add cards to cardsChosen, indexChosen[]
		cardsChosen[currInd] = val;
		indexChosen[currInd] = bIndex;
		
		// sift and sort cardsChosen, indexChosen[]
		siftAndSort(cardsChosen);
		siftAndSort(indexChosen);
		
		checkChosenUpdatePlayButton();
	}
	
	// precondition: called from unselecting a card
	// postcondition: updates interface with deselection
	public void playerUnselect(int val, int bIndex) {
		// find the card in cardsChosen[]
		boolean done = false;
		for (int i=0; i<13 && !done; i++) {
			if (cardsChosen[i] == val) {
				cardsChosen[i] = -1;
				indexChosen[i] = -1;
				done = true;
			}
		}
		currInd--;
		
		
		siftAndSort(cardsChosen);
		siftAndSort(indexChosen);
		
		checkChosenUpdatePlayButton();
	}
	
	// precondition: called from play button
	// postcondition: updates GUI, Game object for making a play
	public void playButton() {
		// make the play in Game object, also animates move
		game.makePlay(cardsChosen, currentPlay);
		
		gAct.playButtonOFF();
		
		if (!game.gameWon) {
			gAct.disablePlayerButtons();
			// run AI's code, and animations
	    	game.runAIRounds();
	    	
	    	// setup player turn upon return from AI rounds
	    	setupPlayerTurn();
		}
	}
	
	/////////////////////////////////////////////////////////////////
	// MISC METHODS
	/////////////////////////////////////////////////////////////////
	
	// precondition: none
	// postcondition: strong cards (low numbers) are at the front
	//		and any -1 in the array are at the end
	public void siftAndSort(int[] cards) {
		// insertion sort is efficient for small data sets like this
		int i, j, newValue;
		for (i=0; i<13; i++) temp[i] = -1;
		// replace -1 with 999 so they go to end during sort
		for (i=0; i<13; i++) {
			if (cards[i] >= 0) 
				temp[i] = cards[i];
			else
				temp[i] = 999;
		}
		
		// sort temp array
		for (i=1; i<13; i++) {
			newValue = temp[i];
			j = i;
	        while (j > 0 && temp[j - 1] > newValue) {
	        	temp[j] = temp[j - 1];
	            j--;
	        }
	        temp[j] = newValue;
		}
		
		// copy back to cards[] (replace 999's with -1)
		for (i=0; i<13; i++) cards[i] = -1;
		boolean endIndexFound = false;
		for (i=0; i<13; i++) {
			if (temp[i] != 999)
				cards[i] = temp[i];
			else {
				if (!endIndexFound) {
					// set index of last real card (non-negative) in cards[]
					currInd = i;
				}
				cards[i] = -1;
				endIndexFound = true;
			}
		}
	}
}