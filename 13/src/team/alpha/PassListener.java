package team.alpha;

import android.view.View;
import android.view.View.OnClickListener;

public class PassListener implements OnClickListener {
	GameActivity gActiv;
	
	public PassListener(GameActivity ga) {
		gActiv = ga;
	}
	
	// precondition: called by Android OS upon user clicking pass button
	// postcondition: calls GameInterface.playerSkipTurn()
	@Override
	public void onClick(View v) {
		gActiv.gInterf.playerSkipTurn();
	}
}