package team.alpha;

import team.alpha.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends Activity {
	// precondition: called by Android OS when creating this Activity
	// postcondition: shows Welcome screen where user can find help or start game
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    
        // set the layout
        setContentView(R.layout.welcome_screen);
        
        // main START button
        Button start = (Button) findViewById(R.id.tien_len_welcome_start_button);
        
        // QUICK SOLUTION
        Button help = (Button) findViewById(R.id.help_welcome_button);
        
        // start button listener
        start.setOnClickListener(new View.OnClickListener() {
        	// anonymous class, override onClick method
        	@Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), GameActivity.class);
                startActivity(myIntent);

            }
        });
        
        help.setOnClickListener(new View.OnClickListener() {
        	// anonymous class, override onClick method
        	@Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), HelpActivity.class);
                startActivity(myIntent);

            }
        });
    }
}