package team.alpha;
public class PLAYS {
	// PLAY constants
	public static final int NOPLAY = -1;
	public static final int PAIR = 0;
	public static final int TRIPS = 1;
	public static final int QUADS = 2;
	public static final int STRAIGHT_3 = 3;
	public static final int STRAIGHT_4 = 4;
	public static final int STRAIGHT_5 = 5;
	public static final int STRAIGHT_6 = 6;
	public static final int STRAIGHT_7 = 7;
	public static final int STRAIGHT_8 = 8;
	public static final int STRAIGHT_9 = 9;
	public static final int STRAIGHT_10 = 10;
	public static final int SEQPAIRS_3 = 11;
	public static final int SEQPAIRS_4 = 12;
	public static final int SEQPAIRS_5 = 13;
	public static final int SINGLE = 14;
}