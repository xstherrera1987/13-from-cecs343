package team.alpha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HelpActivity extends Activity {
	boolean click;
	
	// precondition: none
	// postcondition: called by Android OS when creating this Activity
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.help_activity);
		
		final Button next_slide = (Button)findViewById(R.id.help_screen_one);
		final Button start_game = (Button)findViewById(R.id.start_game_button);
		
	    next_slide.setOnClickListener(new View.OnClickListener() {
        	// anonymous class, override onClick method
        	@Override
            public void onClick(View view) {
        		
        		if(click == true){
        			next_slide.setBackgroundResource(R.drawable.help_one);
        			click = false;
        		}
        		
        		else{
        		
        			click = true;        		
                    next_slide.setBackgroundResource(R.drawable.help_two);
        		}
            }
        });
	    
	    start_game.setOnClickListener(new View.OnClickListener() {
        	// anonymous class, override onClick method
        	@Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), GameActivity.class);
                startActivity(myIntent);

            }
        });
	}
}