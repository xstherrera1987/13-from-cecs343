package team.alpha;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Arrays;

import static team.alpha.PLAYS.*;

public class CLI_Interface implements Serializable {
	// reference to the Game object
	public Game g;
	// the humans' player number
	public final int HUMAN = 0;
	
	public final int ACTION_CANCELSELECTION = -5;
	public final int ACTION_SKIPTURN = -6;
	public final int ACTION_MAKEPLAY = -7;
	public final int ACTION_QUITGAME = -8;
	public final int ACTION_ERROR = -9;
	
	// user input structures, vars
	transient BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	final public int[] cardsChosen = new int[13];
	static final int[] temp = new int[13];
	public int currentPlay = -1;					// holds play type of the cards that are chosen, can display this in GUI
	public int currInd;
	
	// for CLI only (so the debugging user doesn't see a long prompt may times)
	private static int timesPrompted = 0;

	// data structures for printing formatted output
	static final char[] SUITS = {'H', 'D', 'C', 'S'};
	static final char[] RANKS = 
		{'2','A','K','Q','J','X','9','8','7','6','5','4','3'};
	
	public CLI_Interface() {
		// initialize cardsChosen[], temp[]
		for (int i=0; i<13; i++) {
			cardsChosen[i] = -1;
			temp[i] = -1;
		}
		currInd = 0;
	}
	
	/////////////////////////////////////////////////////////////////
	// PLAYER ROUND METHODS
	/////////////////////////////////////////////////////////////////
	
	public void playerRound() {
		currentPlay = g.roundType;
		
		System.out.println("Your cards:");
		printHand(g.hands[HUMAN]);
		System.out.print("Round type=");
		printPlay(currentPlay);
		
		boolean done = false;
		boolean validPlay = false;
		boolean playStronger = false;
		while (!done) {
			// prompt to choose action
			int action = promptMakePlay();
			switch (action) {
			case ACTION_MAKEPLAY:
				validPlay = Game.isCorrectPlay(cardsChosen, currentPlay);
				playStronger = cardsChosen[0] < g.current[0];
				if (validPlay && playStronger) {
					done = true;
					g.makePlay(cardsChosen, currentPlay);
					unchooseAll();
				} else if (validPlay && !playStronger) {
					System.out.println("That is not a stronger play");
				} else {
					System.out.println("That is not a valid play");
				}
				break;
			case ACTION_QUITGAME:
				g.quitGame = true;
				done = true;
				break;
			case ACTION_SKIPTURN:
				g.skipTurn[HUMAN] = true;
				done = true;
				unchooseAll();
				System.out.println("Human#0 skips turn");
				break;
			}
		}
	}
	
	public void playerInitiateRound() {
		System.out.println("Initiate a round");
		printHand(g.hands[HUMAN]);
		
		boolean done = false;
		while (!done) {
			// prompt to choose action
			promptMakePlay();
			// user may have selected quit game here
			if (!g.quitGame) {
				currentPlay = Game.isPlay(cardsChosen);
				// the cards chosen actually make a play
				if (currentPlay != NOPLAY) {
					g.makePlay(cardsChosen, currentPlay);
					unchooseAll();
					done = true;
				} else {
					// NOTE: can't skip turn when initiating a round
					System.out.println("That's not a play");
				}
			} else { // user chose to QUIT the game
				done = true;
			}
		}
	}
	
	public void playerInitiateGame() {
		System.out.println("Initiate the game");
		printHand(g.hands[HUMAN]);
		
		boolean done = false;
		while (!done) {
			// prompt to choose action
			int action = promptMakePlay();
			if (action == ACTION_MAKEPLAY) {
				currentPlay = Game.isPlay(cardsChosen);
				
				// the cards chosen actually make a play
				if (currentPlay != NOPLAY) {
					// check if the play contains #51
					boolean contains51 = false;
					for (int i=0; i<13; i++) {
						if (cardsChosen[i] == 51)
							contains51 = true;
					}
					
					if (contains51) {
						done = true;
						g.makePlay(cardsChosen, currentPlay);
						unchooseAll();
					} else {
						System.out.println("First play must include \"3C\"");
					}
				} else {
					System.out.println("That's not a play");
				}
			} else if (action == ACTION_SKIPTURN) {
				done = true;
				g.skipTurn[HUMAN] = true;
			} else if (action == ACTION_QUITGAME) {
				done = true;
				g.quitGame = true;
			} else {
				System.out.println("ERROR-> playerInitiateGame");
			}
		}
	}

	/////////////////////////////////////////////////////////////////
	// CLI CONTROL FLOW METHODS
	/////////////////////////////////////////////////////////////////
	
	// prompt to make a play, process input, execute command
	public int promptMakePlay() {
		char action;
		int returnAction = -1;
		while (returnAction == -1) {
			action = KB_ChooseAction();
			switch(action) {
			case 'C':	// choose
				promptChooseCard();
				break;
			case 'U': 	// unchoose
				promptUnchooseCard();
				break;
			case 'V':	// view current selection
				System.out.print("current selection: ");
				printHand(cardsChosen);
				break;
			case 'P': 	// make play with selection
				returnAction = ACTION_MAKEPLAY;
				break;
			case 'A':	// unselect all
				unchooseAll();
				System.out.println("All cards unchosen");
				break;
			case 'S':	// skip your turn
				returnAction = ACTION_SKIPTURN;
				break;
			case 'Q':	// quit the game
				returnAction = ACTION_QUITGAME;
			case 'D':	// dump core
				dumpCore();
				break;
			}
		}
		return returnAction;
	}
	
	// precondition: none
	// postcondition: prompts user to select cards for the play
	public void promptChooseCard() {
		// print cards
		System.out.println("CHOOSE");
		printHand(g.hands[HUMAN]);
		
		// prompt user for card string
		int card = KB_CardString();

		if (card != ACTION_CANCELSELECTION) {
			chooseCard(card);
		}
	}
	
	// precondition: none
	// postcondition: prompts user to unselect cards for the play, 
	public void promptUnchooseCard() {
		// print cards
		System.out.println("UNCHOOSE");
		printHand( g.hands[HUMAN] );
		
		// prompt user for card string
		int card = KB_CardString();
		
		// -1 indicates cancel selection
		if (card != -1) {
			unchooseCard(card);
		}
	}

	/////////////////////////////////////////////////////////////////
	// CARD SELECTION METHODS
	/////////////////////////////////////////////////////////////////
		
	// precondition: none
	// postcondition: clears cardsChosen[] for new round
	public void unchooseAll() {
		for (int i=0; i<13; i++) cardsChosen[i] = -1;
		currInd = 0;
	}
	
	// precondition: none
	// postcondition: adds "card" to cardsChosen[], returns true if success, false on error
	public boolean chooseCard(int card) {
		// sifts and sorts so any -1 in cardsChosen percolates to the end
		// also simplifies checking if card is already chosen
		siftAndSort();
		
		// find if this card was already chosen
		boolean inHand = false;
		for (int i=0; cardsChosen[i] != -1; i++) {
			if (card == cardsChosen[i])
				inHand = true;
		}
		
		// check if there is space to choose the card, and it's not already chosen
		if (currInd < 12 && !inHand) {
			cardsChosen[currInd] = (byte) card;
			currInd++;
			siftAndSort();
			return true;
		} else {
			return false;
		}
	}
	
	// precondition: card was previously chosen
	// postcondition: removes "card" from cardsChosen[]
	public void unchooseCard(int card) {
		boolean done=false;
		for (int i=0; i<13 && !done; i++) {
			if (cardsChosen[i] == card) {
				cardsChosen[i] = -1;
				done = true;
			}
		}
		siftAndSort();
	}
	
	// precondition: cards is length 13
	// postcondition: strong cards (low numbers) are at the front
	//		and any -1 in the array are at the end
	public void siftAndSort() {
		// insertion sort is efficient for small data sets like this
		int i, j, newValue;
		for (i=0; i<13; i++) temp[i] = -1;
		// replace -1 with 127 so they go to end during sort
		for (i=0; i<13; i++) {
			if (cardsChosen[i] >= 0) 
				temp[i] = cardsChosen[i];
			else
				temp[i] = 127;
		}
		
		// (insertion) sort temp array
		for (i=1; i<13; i++) {
			newValue = temp[i];
			j = i;
	        while (j > 0 && temp[j - 1] > newValue) {
	        	temp[j] = temp[j - 1];
	            j--;
	        }
	        temp[j] = (byte) newValue;
		}
		
		// copy back to cardsChosen (replace 127's with -1)
		for (i=0; i<13; i++) cardsChosen[i] = -1;
		for (i=0; i<13; i++) {
			if (temp[i] != 127)
				cardsChosen[i] = temp[i];
			else
				cardsChosen[i] = -1;
		}
	}
	
	/////////////////////////////////////////////////////////////////
	// KEYBOARD INPUT METHODS
	/////////////////////////////////////////////////////////////////
	
	public void KB_ChooseSettings() throws IOException {
		String input;		// keyboard input
		int humans = -1;	// number of humans chosen
		
		System.out.println("13 - Choose Settings");
		System.out.println("Choose number of human players:");
		
		boolean done = false;
		input = "";
		while (!done) {
			try {
				input = br.readLine();
			} catch (IOException e) {}
			try {
				humans = Integer.parseInt(input);
			} catch (NumberFormatException e) {}
			if (humans > 0 && humans <= 4)
				done = true;
			else
				System.out.println("Enter an integer (1 through 4)");
		}
	}
	
	public char KB_ChooseAction() {
		boolean done = false;
		String input = "";
		char action = 'X';
		while (!done) {
			if (timesPrompted == 0) {
				System.out.println("Enter \"C\" to Choose a card");
				System.out.println("Enter \"U\" to Unchoose a card");
				System.out.println("Enter \"P\" to play this selection.");
				System.out.println("Enter \"V\" to view current selection");
				System.out.println("Enter \"A\" to unchoose All cards");
				System.out.println("Enter \"S\" to skip your turn");
				System.out.println("Enter \"Q\" to Quit the game");
				System.out.println("Enter \"D\" to dump core");
			} else {
				System.out.println("ENTER COMMAND");
			}
			
			try {
				input = br.readLine();
			} catch (IOException e) {}
			
			action = input.charAt(0);
			
			// if it's any of these, it is valid input
			switch (action) {
			case 'C':
			case 'U':
			case 'P':
			case 'V':
			case 'A':
			case 'S':
			case 'Q':
			case 'D':
				done = true;
				break;
			default:
				System.out.println("Not a command");
				done = false;
				break;
			}
		}
		
		timesPrompted++;
		return action;
	}
	
	// precondition: none
	// postcondition: prompts user for cards to choose, returns integer value of card
	//		or -1 if user wants to cancel selection
	public int KB_CardString() {
		boolean done = false;
		boolean rankValid, suitValid;
		boolean cancel = false;
		String input = "";
		char rank, suit;
		int cardValue = -1;
		
		// prompt user until valid input
		while (!done && !cancel) {
			System.out.println("Enter your selection or \"Z\" to cancel selection");
			try {
				input = br.readLine();
			} catch (IOException e) {}
			
			rankValid = false;
			suitValid = false;
			
			rank = input.charAt(0);
			if (rank == 'Z') {
				cancel = true;
				done = true;
				cardValue = ACTION_CANCELSELECTION;
			} else {
				suit = input.charAt(1);
				
				// check if valid suit
				for (int i=0; i<4; i++) {
					if (suit == SUITS[i]) {
						suitValid = true;
						cardValue = i;
					}
				}
				// check if valid rank
				for (int i=0; i<13; i++) {
					if (rank == RANKS[i]) {
						rankValid = true;
						cardValue += (i*4);
					}
				}
				
				// done if both are valid
				if (rankValid && suitValid)
					done = true;
				else
					System.out.println("Bad input try again:");
			}
		}

		return cardValue;
	}
	
	/////////////////////////////////////////////////////////////////
	// PRINTING METHODS
	/////////////////////////////////////////////////////////////////
	
	// precondition: "i" is a valid card; 0 <= i < 52
	// postcondition: returns a string representing this card
	public static String getCard(int i) {
		char rank, suit;
		rank = RANKS[i/4];
		suit = SUITS[i%4];
		return ""+rank+suit;
	}
	
	// precondition: none
	// postcondition: prints integer value of card string entered at keyboard
	public static void printCardVal_KB() {
		CLI_Interface cli = new CLI_Interface();
		int i = cli.KB_CardString();
		System.out.println(i);
	}
	//	precondition: none
	//	postcondition: prints the cards in each player's hands in a formatted way
	public static void printState(int[] state) {
		char rank, suit;
		
		System.out.println("---------------------------------------");
		for (int h=0; h<4; h++) {
			System.out.println("Player"+h+": ");
			for (int i=0; i<52; i++) {
				if (state[i] == h) {
					rank = RANKS[i/4];
					suit = SUITS[i%4];
					System.out.print(""+rank+suit+' ');
				}
			}
			System.out.println("\n---------------------------------------");
		}
	}
	
	//	precondition: none
	//	postcondition: prints the cards in this hand in a formatted way
	public static void printHand(int[] hand) {
		char rank, suit;
		for (int i=0; i<13; i++) {
			if (hand[i] != -1) {
				rank = RANKS[hand[i] / 4];
				suit = SUITS[hand[i] % 4];
				System.out.print(""+rank+suit+' ');
			}
		}
		System.out.print("\n");
	}
				
	// precondition: play is one of the PLAY constants defined at the top
	// postcondition: prints to console a string describing the play
	public static void printPlay(int play) {
		switch (play) {
		case NOPLAY:
			System.out.println("No Play");
			break;
		case SINGLE:
			System.out.println("Single");
			break;
		case PAIR:
			System.out.println("Pair");
			break;
		case TRIPS:
			System.out.println("Triple");
			break;
		case QUADS:
			System.out.println("Four-of-a-kind");
			break;
		case STRAIGHT_3:
			System.out.println("Straight-of-3");
			break;
		case STRAIGHT_4:
			System.out.println("Straight-of-4");
			break;
		case STRAIGHT_5:
			System.out.println("Straight-of-5");
			break;
		case STRAIGHT_6:
			System.out.println("Straight-of-6");
			break;
		case STRAIGHT_7:
			System.out.println("Straight-of-7");
			break;
		case STRAIGHT_8:
			System.out.println("Straight-of-8");
			break;
		case STRAIGHT_9:
			System.out.println("Straight-of-9");
			break;
		case STRAIGHT_10:
			System.out.println("Straight-of-10");
			break;
		case SEQPAIRS_3:
			System.out.println("Sequence-of-3-Pairs");
			break;
		case SEQPAIRS_4:
			System.out.println("Sequence-of-4-Pairs");
			break;
		case SEQPAIRS_5:
			System.out.println("Sequence-of-5-Pairs");
			break;
		}
	}

	// print lots of debugging data to console 
	public void dumpCore() {
		System.out.println("GAME STATE");
		printState(g.state);
		for (int i=0; i<4; i++) {
			System.out.print("hands["+i+"]= ");
			printHand(g.hands[i]);
		}
		System.out.print("roundType= ");
		printPlay(g.roundType);
		
		System.out.print("cardsChosen[]= ");
		printHand(cardsChosen);
		
		for (int i=0; i<4; i++) {
			System.out.print("skipTurn["+i+"]= ");
			System.out.println(g.skipTurn[i]);
		}
	}
	
	public void dumpAI_Core() {
		for (int i=0; i<8; i++) {
			Arrays.toString(g.playsFound[i]);
		}
	}
	
	static final long serialVersionUID = 987654321L;
}