package team.alpha;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LostGameActivity extends Activity {
	// precondition: none
	// postcondition: called by Android OS when creating this Activity
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lost_game_screen);
		Button play_again = (Button)findViewById(R.id.play_again_from_lost);
		
		play_again.setOnClickListener(new View.OnClickListener() {
        	// anonymous class, override onClick method
        	@Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), GameActivity.class);
                startActivity(myIntent);

            }
        });
		
		Button quit = (Button)findViewById(R.id.quit_from_lost);
		quit.setOnClickListener(new View.OnClickListener() {
        	// anonymous class, override onClick method
        	@Override
            public void onClick(View view) {
        		finish();
        		android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}
}