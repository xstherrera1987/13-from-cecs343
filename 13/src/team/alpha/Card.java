package team.alpha;
public class Card {
	// the value of this card (0-51)
	public int val;
	// R.java reference to the resource of this card
	public int res;
	public Card(int val) {
		this.val = val;
	}
}