package team.alpha;

import android.view.View;
import android.view.View.OnClickListener;

public class PlayListener implements OnClickListener {
	GameActivity gAct;
	
	public PlayListener(GameActivity gameActivity){
		gAct = gameActivity;
	}
	
	// precondition: called by Android OS upon user clicking play button
	// postcondition: sets clicked[] to reflect this button's state
	//		and calls GameInterface.playerSelect() or playerUnselect()
	public void onClick(View v) {
		gAct.gInterf.playButton();
	}
}