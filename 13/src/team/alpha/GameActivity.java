package team.alpha;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class GameActivity extends Activity  {
	// display variables
    RelativeLayout relLayout;
	Display disp;
	int height, width;
	
	// UI components
	boolean[] clicked;
	ImageButton[] buttons; 				// reference to each user button
	ImageButton[] current_played;			// current_played holds the played cards
	ImageButton play, pass;				// play and pass button
	ImageView[][] AI_Cards;
    ImageButton[] pass_message_vertical;
    ImageButton[] pass_message_horizontal;				// references to all the AI cards
	
	// gameplay structures
	Card[] cards; 						// holds resource and value for all the cards
	GameInterface gInterf;				// game interface reference
    TranslateAnimation up[], down[]; 	// up and down animations for each button in user's hand
    int visibleCards[]; 				// values of the cards in the players hand
    int[] cardsLeft;					// how many cards are left in each player's hand
	public final int HUMAN = 0;			// by default human is player #0
    	
	// music
	private static MediaPlayer music;
	
	// precondition: called by Android OS when creating this Activity
	// postcondition: initialises userinterface, gameplay structures,
	//		and starts the game
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// user interface calculation and initialization
		initUserInterface();
		
		// gameplay structure initialization
		initGameplayStructures();
        
		initMusic();
		
        // start the game logic
		gInterf.startGame();
	}
	
	// precondition: called by Android OS when resuming/creating this Activity
	// postcondition: restarts music
	public void onResume() {
		super.onResume();
		music.start();
	}
	
	// precondition: called by Android OS when user pauses this Activity
	// postcondition: pauses the music
	public void onPause() {
		super.onPause();
		music.pause();
	}
	
	// precondition: called by Android OS when application is terminated
	// postcondition: releases resources for the mp3 file
	public void onDestroy() {
		super.onDestroy();
		music.release();
	}
	
	/////////////////////////////////////////////////////////////////
	// INITIALIZATION METHODS
	/////////////////////////////////////////////////////////////////
	
	// precondition: none
	// postcondition: initializes the user interface components
	public void initUserInterface() {
		disp = getWindowManager().getDefaultDisplay();
		height = disp.getHeight();
		width = disp.getWidth();
		
		
		relLayout = new RelativeLayout(this);		
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
										      RelativeLayout.LayoutParams.FILL_PARENT,
										      RelativeLayout.LayoutParams.FILL_PARENT);		
		relLayout.setLayoutParams(rlp);		
		relLayout.setBackgroundResource(R.drawable.table_two);
		
		RelativeLayout.LayoutParams[] left_AI_cards = new RelativeLayout.LayoutParams[13];
		RelativeLayout.LayoutParams[] right_AI_cards = new RelativeLayout.LayoutParams[13];
		RelativeLayout.LayoutParams[] top_AI_cards = new RelativeLayout.LayoutParams[13];
		RelativeLayout.LayoutParams[] user_cards_param = new RelativeLayout.LayoutParams[13];

		RelativeLayout.LayoutParams[] current_cards_params = new RelativeLayout.LayoutParams[10];		
		RelativeLayout.LayoutParams[] pass_msg_vert_cards = new RelativeLayout.LayoutParams[2];
		RelativeLayout.LayoutParams[] pass_msg_horz_cards = new RelativeLayout.LayoutParams[2];

//position the pass messages
		
		//pass_message_vertical
		int shiftPassMessagesVert = 705;
		for(int i = 0 ; i < 2 ; i++){
			pass_msg_vert_cards[i] = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
		             												LayoutParams.WRAP_CONTENT);
			pass_msg_vert_cards[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			pass_msg_vert_cards[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			pass_msg_vert_cards[i].leftMargin = width - shiftPassMessagesVert;
			pass_msg_vert_cards[i].topMargin = height - 325;
			shiftPassMessagesVert = shiftPassMessagesVert - 570;
			
			
		}
		
		//initializing pass_message_vertical
		pass_message_vertical = new ImageButton[2];
		pass_message_horizontal = new ImageButton[2];
		for(int i = 0 ; i < 2 ; i++){
			pass_message_vertical[i] = new ImageButton(this);
			if(i == 0){
			pass_message_vertical[i].setBackgroundResource(R.drawable.pass_left);
			}
			if(i == 1){
		    pass_message_vertical[i].setBackgroundResource(R.drawable.pass_right);
			}
			pass_message_vertical[i].setVisibility(View.INVISIBLE);
			pass_message_vertical[i].setLayoutParams(pass_msg_vert_cards[i]);
			relLayout.addView(pass_message_vertical[i]);
		}
		
	    //pass_message_horizontal
		int shiftPassMessagesHorz = 390;
		for(int i = 0 ; i < 2 ; i++){
			pass_msg_horz_cards[i] = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
		             												LayoutParams.WRAP_CONTENT);
			pass_msg_horz_cards[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			pass_msg_horz_cards[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			pass_msg_horz_cards[i].leftMargin = width - 450;
			pass_msg_horz_cards[i].topMargin = height - shiftPassMessagesHorz;
			shiftPassMessagesHorz = shiftPassMessagesHorz - 190;
			
			
		}
		
		//initializing pass_message_horizontal
		
		for(int i = 0 ; i < 2 ; i++){
			pass_message_horizontal[i] = new ImageButton(this);
			if(i == 0){
			pass_message_horizontal[i].setBackgroundResource(R.drawable.pass_top);
			}
			if(i == 1){
		    pass_message_horizontal[i].setBackgroundResource(R.drawable.pass_human);
			}
			pass_message_horizontal[i].setVisibility(View.INVISIBLE);
			pass_message_horizontal[i].setLayoutParams(pass_msg_horz_cards[i]);
			relLayout.addView(pass_message_horizontal[i]);
		}
		
		current_played = new ImageButton[10];
		//positioning Current Cards
		int shiftCurrentCards = width - 585;
		for(int i = 0 ; i < 10 ; i++){
			current_cards_params[i] = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
	                									             LayoutParams.WRAP_CONTENT);
			current_cards_params[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			current_cards_params[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			
			current_cards_params[i].topMargin = height - 300;
			current_cards_params[i].leftMargin = shiftCurrentCards;
			shiftCurrentCards = shiftCurrentCards + 35;
		}

       //current cards
        for(int i = 0 ; i < 10 ; i++){
        	current_played[i] = new ImageButton(this);
        	current_played[i].setBackgroundResource(R.drawable.card_face_down);
        	current_played[i].setClickable(false);
        	current_played[i].setVisibility(View.INVISIBLE);
        	current_played[i].setLayoutParams(current_cards_params[i]);
        	relLayout.addView(current_played[i]);
        	
        }
		
		// for human's cards
		int shiftUserLeft = 175;
		for(int i = 0 ; i < 13 ; i++){
			user_cards_param[i] = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
	                											LayoutParams.WRAP_CONTENT);
			user_cards_param[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			user_cards_param[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			
			user_cards_param[i].topMargin = height - 150;			
			user_cards_param[i].leftMargin = shiftUserLeft;
			shiftUserLeft = shiftUserLeft + 35;
		}
		
		//for left AI Cards
		int shiftLeftCard = 45;
		for(int i = 0 ; i < 13 ; i++){
			left_AI_cards[i] =  new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
	                											LayoutParams.WRAP_CONTENT);
			left_AI_cards[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			left_AI_cards[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			
			left_AI_cards[i].topMargin = shiftLeftCard;
			left_AI_cards[i].leftMargin = 25;
			shiftLeftCard = shiftLeftCard + 20;
		}
		//for right AI Cards
		int shiftRightCard = 45;
		for(int i = 0 ; i < 13 ; i++){
			right_AI_cards[i] =  new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
	                											LayoutParams.WRAP_CONTENT);
			right_AI_cards[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			right_AI_cards[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			
			right_AI_cards[i].topMargin = shiftRightCard;
			right_AI_cards[i].leftMargin = width - 85;
			shiftRightCard = shiftRightCard + 20;
		}
		//for top AI Cards
		int shiftTopCard = 175;
		for(int i = 0 ; i < 13 ; i++){
			top_AI_cards[i] = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
																LayoutParams.WRAP_CONTENT);
			top_AI_cards[i].addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			top_AI_cards[i].addRule(RelativeLayout.ALIGN_PARENT_TOP);
			
			top_AI_cards[i].topMargin = 20;
			top_AI_cards[i].leftMargin = shiftTopCard;
			shiftTopCard = shiftTopCard + 35;
		}
		
		// initialise AI_cards[]
		// AI#1 is right, AI#2 is top, AI#3 is left
		AI_Cards = new ImageView[4][52];
		for (int i=0; i<13; i++) {
			AI_Cards[1][i] = new ImageView(this);
			AI_Cards[1][i].setBackgroundResource(R.drawable.side_card_face_down);
			AI_Cards[1][i].setLayoutParams(right_AI_cards[i]);
		}
		for (int i=0; i<13; i++) {
			AI_Cards[2][i] = new ImageView(this);
			AI_Cards[2][i].setBackgroundResource(R.drawable.card_face_down);
			AI_Cards[2][i].setLayoutParams(top_AI_cards[i]);
		}
		for (int i=0; i<13; i++) {
			AI_Cards[3][i] = new ImageView(this);
			AI_Cards[3][i].setBackgroundResource(R.drawable.side_card_face_down);
			AI_Cards[3][i].setLayoutParams(left_AI_cards[i]);
		}
		for (int i = 13 ; i < 52 ; i++) {
			relLayout.addView( AI_Cards[ i / 13 ][ i % 13 ] );
		}
		
		
		// initialise player's cards buttons
		buttons = new ImageButton[13];
		clicked = new boolean[13];
		for (int i = 0 ; i < 13 ; i++) {
			buttons[i] = new ImageButton(this);			
			buttons[i].setOnClickListener(new SelectListener(i, this));
			buttons[i].setBackgroundResource(R.drawable.card_face_down);
			buttons[i].setClickable(true);			
			buttons[i].setLayoutParams(user_cards_param[i]);
			clicked[i] = false;
		}
		
        play = new ImageButton(this);
        pass = new ImageButton(this);
        
        RelativeLayout.LayoutParams playBtnParam = new RelativeLayout.LayoutParams
        		(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        playBtnParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        playBtnParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        playBtnParam.leftMargin = 100;
        playBtnParam.topMargin = height - 155;
        play.setOnClickListener(new PlayListener(this) );
        play.setBackgroundResource(R.drawable.play_one_green);
        play.setLayoutParams(playBtnParam);
        relLayout.addView(play);
        
        RelativeLayout.LayoutParams passButtonParam = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,  
				  LayoutParams.WRAP_CONTENT);
        
        passButtonParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        passButtonParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        passButtonParam.leftMargin = width - 135;
        passButtonParam.topMargin = height - 155;
        pass.setOnClickListener(new PassListener(this));
        pass.setBackgroundResource(R.drawable.pass_one_green);
        pass.setLayoutParams(passButtonParam);
        
        relLayout.addView(pass);
        
        
        
        // initialize central current played buttons
        //current_played = new Button[10];
        // TODO: passBtnParam = ..
        //	passBtn.addRule....
        //	passBtn.setOnClickListener...
        //  relLayout.addView(passBtn)...
        
		
		// initialize animations
		up = new TranslateAnimation[13];
		down = new TranslateAnimation[13];
		for (int i = 0; i < 13; i++) {
			down[i] = new TranslateAnimation(0,0,-10,0);
			down[i].setFillAfter(false);
		}
		for (int i = 0; i < 13; i++) {
			up[i] = new TranslateAnimation(0,0,0,-10);
			up[i].setFillAfter(true);
		}
		
		// set this layout onto the content view
		setContentView(relLayout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}
	
	// precondition: none
	// postcondition: initializes the gameplay structures
	public void initGameplayStructures() {
		gInterf = new GameInterface(this);
		visibleCards = new int[13];
		cards = new Card[52];
		cardsLeft = new int[4];
		
		for (int i=0; i<4; i++)
			cardsLeft[i] = 13;
		
        // initialize cards[]
		for (int i=0; i<52; i++)
        	cards[i] = new Card(i);
		
		// set the resources for cards[]
        cards[0].res = R.drawable.two_of_hearts;
        cards[1].res = R.drawable.two_of_diamonds;
        cards[2].res = R.drawable.two_of_clubs;
        cards[3].res = R.drawable.two_of_spades;
        cards[4].res = R.drawable.ace_of_hearts;
        cards[5].res = R.drawable.ace_of_diamonds;
        cards[6].res = R.drawable.ace_of_clubs;
        cards[7].res = R.drawable.ace_of_spades_1;
        cards[8].res = R.drawable.king_of_hearts_1;
        cards[9].res = R.drawable.king_of_diamonds_1;
        cards[10].res = R.drawable.king_of_spades_1;
        cards[11].res = R.drawable.king_of_clubs_1;
        cards[12].res = R.drawable.queen_of_hearts_1;
        cards[13].res = R.drawable.queen_of_diamonds_1;
        cards[14].res = R.drawable.queen_of_clubs_1;
        cards[15].res = R.drawable.queen_of_spades_1;
        cards[16].res = R.drawable.jack_of_hearts_1;
        cards[17].res = R.drawable.jack_of_diamonds_1;
        cards[18].res = R.drawable.jack_of_clubs_1;
        cards[19].res = R.drawable.jack_of_spades_1;
        cards[20].res = R.drawable.ten_of_hearts;
        cards[21].res = R.drawable.ten_of_diamonds;
        cards[22].res = R.drawable.ten_of_clubs;
        cards[23].res = R.drawable.ten_of_spades;
        cards[24].res = R.drawable.nine_of_hearts;
        cards[25].res = R.drawable.nine_of_diamonds;
        cards[26].res = R.drawable.nine_of_clubs;
        cards[27].res = R.drawable.nine_of_spades;
        cards[28].res = R.drawable.eight_of_hearts;
        cards[29].res = R.drawable.eight_of_diamonds;
        cards[30].res = R.drawable.eight_of_clubs;
        cards[31].res = R.drawable.eight_of_spades;
        cards[32].res = R.drawable.seven_of_hearts;
        cards[33].res = R.drawable.seven_of_diamonds;
        cards[34].res = R.drawable.seven_of_clubs;
        cards[35].res = R.drawable.seven_of_spades;
        cards[36].res = R.drawable.six_of_hearts;
        cards[37].res = R.drawable.six_of_diamonds;
        cards[38].res = R.drawable.six_of_clubs;
        cards[39].res = R.drawable.six_of_spades;
        cards[40].res = R.drawable.five_of_hearts;
        cards[41].res = R.drawable.five_of_diamonds;
        cards[42].res = R.drawable.five_of_clubs;
        cards[43].res = R.drawable.five_of_spades;
        cards[44].res = R.drawable.four_of_hearts;
        cards[45].res = R.drawable.four_of_diamonds;
        cards[46].res = R.drawable.four_of_clubs;
        cards[47].res = R.drawable.four_of_spades;
        cards[48].res = R.drawable.three_of_hearts;
        cards[49].res = R.drawable.three_of_diamonds;
        cards[50].res = R.drawable.three_of_clubs;
        cards[51].res = R.drawable.three_of_spades;
	}
	
	// precondition : hand[] is the initial cards in the players hand
	// postcondition: initialize visibleCard[] and sets buttons[] resources
	public void initHumansHand(int[] hand) {
		int currCard;
		for (int i = 0 ; i<13 ; i++) {
			currCard = hand[i];
			visibleCards[i] = currCard;
			buttons[i].setBackgroundResource( cards[currCard].res );			
			buttons[i].setVisibility( View.VISIBLE );
			buttons[i].setClickable(true);
			relLayout.addView( buttons[i] );
		}
	}
	
	// precondition : none
	// postcondition: disables the user's buttons during AI moves
	public void disablePlayerButtons() {
		// make cards buttons un clickable
		for (int i=0; i<13; i++) {
			buttons[i].setClickable(false);
		}
		
		// play and pass buttons unclickable
		play.setClickable(false);
		pass.setClickable(false);
	}
	
	// precondition : none
	// postcondition: enables the user's buttons after AI moves
	public void enablePlayerButtons() {
		for (int i=0; i<13; i++) {
			// if its -1, it's no longer in hand
			if (visibleCards[i] != -1)
				buttons[i].setClickable(true);
		}
	}
	
	// precondition: none
	// postcondition: loads the mp3 file
	public void initMusic() {
		music = MediaPlayer.create(this, R.raw.song);
	}
	
	/////////////////////////////////////////////////////////////////
	// ANIMATION METHODS
	/////////////////////////////////////////////////////////////////
	
	// precondition: cardsChosen[] is the valid play, indicesChosen[] is correct
	// postcondition: animates the moving of cards
	public void animateHumanPlay() {
		// index of the button that was clicked
    	int bIndex;
		// value of card on button that was clicked
		int card;
    	for(int i = 0 ; i < gInterf.currInd ; i++) {
    		// get values for chosen cards/indices
    		card = gInterf.cardsChosen[i];
    		bIndex = gInterf.indexChosen[i];
    		
    		// set the image on current_played[]
    		current_played[i].setBackgroundResource(cards[card].res);
    		// set current played to visible
    		current_played[i].setVisibility(View.VISIBLE);
    	
    		// set the cards previously in hand to invisible
    		buttons[bIndex].setVisibility(View.INVISIBLE);
    		// animate them down to force redraw
    		buttons[bIndex].startAnimation(down[bIndex]);
    	}
	}
	
	// precondition: play[] is the hand to move to center, playerNumber
	//		is the AI's player number
	// postcondition: moves this AI's cards to the center of the table
	public void animateAIPlay(int[] play, int playerNumber) {
		// clear previous hand
		for (int i=0; i<10; i++) {
			current_played[i].setVisibility(View.INVISIBLE);
		}
		
		// the amount of cards in the play
		int amt = 0;
		for (int i=0; i<13 && play[i] != -1; i++) {
			amt += 1;
		}
		
		// set "amt" cards invisible
		int lastCard = cardsLeft[playerNumber] - 1;
		for (int i = lastCard; i > lastCard - amt; i--) {
			AI_Cards[playerNumber][i].setVisibility(View.INVISIBLE);
		}
		
		cardsLeft[playerNumber] -= amt;
		
		// show them in center
		int card;
		for (int i=0; i < amt; i++) {
			card = play[i];
			current_played[i].setBackgroundResource(cards[card].res);
			current_played[i].setVisibility(View.VISIBLE);
		}
	}

	// precondition: player is the player# of the one skipping turn
	// postcondition: turns on this player's SKIP indicator
	public void skipTurn(int player) {
		switch (player) {
		case HUMAN: 
			//turn on skip button
			pass_message_horizontal[1].setVisibility(View.VISIBLE);
			break;
		case 1: 
			//turn on skip button
			pass_message_vertical[1].setVisibility(View.VISIBLE);
			break;
		case 2: 
			pass_message_horizontal[0].setVisibility(View.VISIBLE);
			break;
		case 3: 
			pass_message_vertical[0].setVisibility(View.VISIBLE);
			break;
		}
	}
	
	// precondition: none
	// postcondition: turns off all players' SKIP indicators
	public void clearSkipButtons() {
		pass_message_horizontal[0].setVisibility(View.INVISIBLE);
		pass_message_horizontal[1].setVisibility(View.INVISIBLE);
		pass_message_vertical[1].setVisibility(View.INVISIBLE);
		pass_message_vertical[0].setVisibility(View.INVISIBLE);
	}
	
	// precondition: none
	// postcondition: turns pass button off
	public void passButtonON() {
		pass.setBackgroundDrawable(play.getContext().getResources().getDrawable(R.drawable.pass_one_white));
		pass.setClickable(true);
	}
	
	// precondition: none
	// postcondition: turns pass button on
	public void passButtonOFF() {
		pass.setBackgroundDrawable(play.getContext().getResources().getDrawable(R.drawable.pass_one_green));
		pass.setClickable(false);
	}
	
	// precondition: none
	// postcondition: turns play button on
	public void playButtonON() {
		play.setBackgroundDrawable(play.getContext().getResources().getDrawable(R.drawable.play_one_white));
		play.setClickable(true);
	}
	
	// precondition: none
	// postcondition: turns play button off
	public void playButtonOFF() {
		play.setBackgroundDrawable(play.getContext().getResources().getDrawable(R.drawable.play_one_green));
		play.setClickable(false);
	}

	// precondition: none
	// postcondition: initiates the proper activity (win/lose) depending on
	//		which player won the game
	public void gameWon(int player) {
		if (player == HUMAN) {
			// launch WonGame Activity
            Intent myIntent = new Intent(getApplicationContext(), WonGameActivity.class);
            startActivity(myIntent);
            finish();
		} else {
			// launch LostGame Activity
            Intent myIntent = new Intent(getApplicationContext(), LostGameActivity.class);
            startActivity(myIntent);
            finish();
		}
	}
	
	// precondition: none
	// postcondition: clears the cards at the center of the table
	public void clearCurrentPlayed() {
		for (int i=0; i<10; i++) {
			current_played[i].setVisibility(View.INVISIBLE);
		}
	}
}
